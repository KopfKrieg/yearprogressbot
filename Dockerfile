# Dockerfile References: https://docs.docker.com/engine/reference/builder/

# Start from golang v1.11 base image
FROM golang:latest

# Add Maintainer Info
LABEL maintainer="Florian \"KopfKrieg\" Krauskopf <dev@absolem.cc>"

# Set the Current Working Directory inside the container
WORKDIR $GOPATH/src/gitlab.com/kopfkrieg/yearprogressbot

# Copy everything from the current directory to the PWD(Present Working Directory) inside the container
COPY . .

# Download all the dependencies
# https://stackoverflow.com/questions/28031603/what-do-three-dots-mean-in-go-command-line-invocations
RUN go get -d -v ./...

# Install the package
RUN go install -v ./...

# Run the executable
CMD ["yearprogressbot"]

